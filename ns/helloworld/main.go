package main

import (
	"gitlab.com/eper.io/engine/metadata"
	"net/http"
	"strings"
)

func main() {
	if strings.HasPrefix(metadata.SiteUrl, "https://") {
		http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
			_, _ = writer.Write([]byte("Hello World!"))
		})
		go func() {
			err := http.ListenAndServe(":80", http.RedirectHandler(metadata.SiteUrl, http.StatusTemporaryRedirect))
			if err != nil {
				panic("ListenAndServe: " + err.Error())
			}
		}()
		err := http.ListenAndServeTLS(":443", metadata.Certificate, metadata.PrivateKey, nil)
		if err != nil {
			panic("ListenAndServeTLS: " + err.Error())
		}
	}
}
